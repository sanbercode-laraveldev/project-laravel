<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request){
        $namaDepan = $request['namaDepan'];
        $namaBelakang = $request['namaBelakang'];
        $kebangsaan = $request['kebangsaan'];
        $bio = $request['bio'];

        return view('welcome',compact('namaDepan','namaBelakang','kebangsaan','bio'));
    }
    public function register(){
        return view("register");
    }
}
