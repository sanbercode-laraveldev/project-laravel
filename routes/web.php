<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Templating Laravel //

// rute untuk form index
Route::get('/', [HomeController::class, "index"]);
// rute untuk form register
Route::get('/register', [AuthController::class, 'register']);
// rute utuk form welcome
Route::post('/welcome', [AuthController::class, 'welcome']);

//Table View //

// form table 1
Route::get('/table', function () {
    return view('table');
});
// form table 2
Route::get('/data-table', function () {
    return view('datatable');
});

// CRUD category //

// Create
// form tambah data ke database
Route::get('/cast/create', [CastController::class, 'create']);
// rute tambah data 
Route::post('/cast', [CastController::class, 'store']);

// Read
// form tampilkan seluruh data dari database
Route::get('/cast', [CastController::class, 'read']);
// rute fungsi tampilkan data berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update
// form update data berdasar id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// rute fungsi update data berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete
// rute fungsi delete data berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);