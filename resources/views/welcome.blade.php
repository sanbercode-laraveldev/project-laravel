@extends('layout.master')

@section('judul')
Selamat Datang
@endsection

@section('content')
<h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}!</h1>
<h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama! </h2>
<br>
<p>Data kamu yang kami simpan adalah sebagai berikut:</p>
    <ul>
        <li>Nama Lengkap : {{$namaDepan}} {{$namaBelakang}}</li>
        <li>Kebangsaan : {{$kebangsaan}} </li>
        <li>Bio : {{$bio}} </li>
@endsection

