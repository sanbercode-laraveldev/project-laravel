@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
    @csrf
    <label>First Name: <br> <br>
        <input type="text" name="namaDepan">
    </label> <br> <br>
    <label>Last Name: <br> <br>
        <input type="text" name="namaBelakang">
    </label> <br> <br>
    <label>Gender: <br> <br>
        <input name="male" type="radio"> Male <br>
        <input name="female" type="radio"> Female <br>
        <input name="other" type="radio"> Other
    </label> <br> <br>
    <label>Nationality: <br> <br>
        <select name="kebangsaan">
            <option value="Indonesian">Indonesian</option>
            <option value="Asian">Asian</option>
            <option value="American">American</option>
            <option value="European">European</option>
            <option value="African">African</option>
        </select>
    </label> <br> <br>
    <label>Language Spoken: <br> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Others
    </label> <br> <br>
    <label>Bio: <br> <br>
        <textarea cols="30" rows="8" name="bio"> </textarea>
    </label> <br>
    <input type="submit" value="Sign Up">
</form>

@endsection
