@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('content')
    {{-- ambil compat data nama --}}
    <h1>{{ $cast->nama }}</h1>
    {{-- ambil compat data umur --}}
    <h4>{{ $cast->umur }} Tahun</h4>
    {{-- ambil compat data bio --}}
    <p>{{ $cast->bio }}</p>
    {{-- button untuk rute kembali --}}
    <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection
