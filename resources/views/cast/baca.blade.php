@extends('layout.master')

@section('judul')
    List Cast
@endsection

@section('content')
    {{-- tombol link untuk menuju rute form tambah data --}}
    <a href="/cast/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
    <table class="table table-dark table-sm">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            {{-- perulangan untuk menampilkan data dari database --}}
            @forelse ($cast as $key => $value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->nama }}</td>
                    <td>{{ $value->umur }}</td>
                    <td>
                        {{-- form untuk tombol hapus data dari database 
                            dan berisi tombol link untuk menuju rute form Detail dan Edit --}}
                        <form action="/cast/{{ $value->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="4">Tidak Ada Data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
