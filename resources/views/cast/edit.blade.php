@extends('layout.master')

@section('judul')
    Edit Detail Cast
@endsection

@section('content')
    {{-- form untuk update data baru ke database --}}
    <form action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control form-control-sm">
        </div>

        {{-- validation untuk input nama --}}
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Umur</label>
            <input type="number" value="{{ $cast->umur }}" min="0" max="100" name="umur"
                class="form-control form-control-sm">
        </div>

        {{-- validation untuk input umur --}}
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control form-control-sm" cols="30" rows="10"><?php echo htmlspecialchars($cast->bio); ?></textarea>
        </div>

        {{-- validation untuk input bio --}}
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- tombol untuk rute batal dan rute submit --}}
        <a href="/cast" class="btn btn-primary btn-sm">Batal</a>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    </form>
@endsection
